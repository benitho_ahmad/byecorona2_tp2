from django.test import LiveServerTestCase, TestCase, Client, tag
from django.urls import reverse
from selenium import webdriver
from .models import pengunjung

class TestMain(TestCase):
    def test_url_slash_main(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)

    def test_url_slash_savedata(self):
        response = Client().get('/savedata/')
        self.assertEquals(response.status_code, 200)

    def test_cek_hasil_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'main/home.html')

    def test_model_pengunjung(self):
        pengunjung.objects.create(Visitor_Name='user123', Email='user123@gmail.com')
        check_masuk = pengunjung.objects.count()
        self.assertEquals(check_masuk, 1)
        self.assertEquals(pengunjung.Visitor_Name, 'user123')
        self.assertEquals(pengunjung.Email, 'user123@gmail.com')

