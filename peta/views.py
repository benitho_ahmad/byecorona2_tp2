from django.shortcuts import render, redirect
from .models import Feedback
from .forms import FeedbackForm
from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
import requests
import json


def feedback(request):
    responses = {}
    if request.method == 'POST':
        if request.is_ajax():
            if request.user.is_authenticated:
                form = FeedbackForm(request.POST)
                if form.is_valid():
                    name = request.user.username
                    feedback = form.cleaned_data['feedback']
                    Feedback(name=name, feedback=feedback).save()
                    return JsonResponse({'success': True})
        return JsonResponse({'success': False})

    else:
        form = FeedbackForm()
        responses['form'] = form
        return render(request, 'peta/peta.html', responses)

def fungsi_data(request):
    data = {
        'data':[i.get_dict() for i in Feedback.objects.all()[::-1][:5]],
    }
    return JsonResponse(data=data)
