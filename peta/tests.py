from django.test import TestCase, Client
from http import HTTPStatus
from django.urls import resolve, reverse
from .models import Feedback
from . import views
from .forms import FeedbackForm
from django.contrib.auth.models import User
import json

# # Create your tests here.
# class feedbackHTMLTest(TestCase):
#     def setUp(self):
#         User.objects.create_user("siapa", "siapa")

#     def test_text(self):
#         response = self.client.get('/peta/feedback/')
#         self.assertIn('Login Dulu!', response.content.decode())

#     def test_submit_button(self):
#         response = self.client.get('/peta/feedback/')
#         self.assertNotIn('Submit', response.content.decode())

#         self.client.login(username="siapa", password="123abdse")
#         response = self.client.get('/peta/feedback/')
#         self.assertIn('Submit', response.content.decode())

#     def test_form(self):
#         self.client.login(username="siapa", password="123abdse")
#         response = self.client.get('/peta/feedback/')
#         self.assertIn('<form', response.content.decode())
#         self.assertIn('<input', response.content.decode())


# class FeedbackViewsTest(TestCase):
#     def setUp(self):
#         User.objects.create_user("siapa", "THIS IS MESSAGE")

#     def test_form_valid(self):
#         headers = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
#         response = self.client.post('/peta/feedback/', **headers, data={
#             'feedback': 'THIS IS MESSAGE',
#         })
#         self.assertTrue(Feedback.objects.filter(feedback="THIS IS MESSAGE").exists())

#     def test_form_invalid(self):
#         headers = {'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
#         response = self.client.post('/peta/feedback/', **headers, data={
#             'feedback': 'THIS IS MESSAGE',
#         })
#         self.assertFalse(Feedback.objects.filter(feedback="THIS IS MESSAGE").exists())

class FeedbackModelTest(TestCase):
    def setUp(self):
        data = Feedback(
            name="siapa",
            feedback="THIS IS MESSAGE!",
        )
        data.save()

    def test_model_peta(self):
        hitung = Feedback.objects.all().count()
        self.assertEquals(hitung, 1)

    def test_get_JSON_correct(self):
        response = self.client.get("/peta/fungsi_data/")
        feedback = Feedback.objects.all()[::-1][:5]
        self.assertEqual(len(json.loads(response.content)['data']), len(feedback))
