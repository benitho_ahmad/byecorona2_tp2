from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.feedback, name='feedback'),
    path('fungsi_data/', views.fungsi_data, name='fungsi_data'),
]
