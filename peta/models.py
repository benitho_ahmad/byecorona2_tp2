from django.db import models

# Create your models here.
class Feedback(models.Model):
    name = models.CharField(max_length=200)
    feedback = models.CharField(max_length=200)

    def get_dict(self):
        return {
            'name':self.name,
            'feedback':self.feedback
        }
