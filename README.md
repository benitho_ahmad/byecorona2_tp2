[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

## Nama-Nama Anggota Kelompok :
1. Benitho Ahmad Suryo
2. Daniel Fernando Parulian Simangunsong
3. Iqra Islamaddina Farhan
4. Muhammad Rakha Zachrie

## Link Herokuapp

http://byecorona2.herokuapp.com/

## Cerita Aplikasi Beserta Manfaatnya

ByeCorona adalah website yang berisi informasi-informasi yang berhubungan dengan virus Covid-19.
Website ini menyediakan berita terbaru tentang covid-19, infografis Covid-19 di Indonesia, 
cara menghindari Covid-19, gejala yang dialami apabila terinfeksi virus Covid-19, dan informasi
mengenai rumah sakit rujukan khusus Covid-19.

## Daftar Fitur Yang Diimplementasikan

1. Halaman Home
   Berisi tampilan home dari website ByeCorona yang menampilkan pengertian dari virus
   Covid-19.

2. Halaman News
   Berisi berita-berita terkini mengenai virus Covid-19.

3. Halaman Infographics
   Berisi statistik mengenai Covid-19 yang ada di Indonesia.

4. Halaman Preventions
   Berisi informasi mengenai cara mencegah tertular/menularkan virus Covid-19 dan
   gejala apa saja jika terinfeksi Covid-19.

5. Halaman Maps
   Berisi informasi Google Maps mengenai RS mana saja yang merupakan RS rujukan untuk
   Covid-19 beserta nomor teleponnya.

6. Menu Navigasi

7. Footer
   Berisi helpful links dan resource data yang digunakan pada website ByeCorona.




[pipeline-badge]: https://gitlab.com/benitho_ahmad/byecorona2_tp2/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/benitho_ahmad/byecorona2_tp2/badges/master/coverage.svg
[commits-gl]: https://gitlab.com/benitho_ahmad/byecorona2_tp2/-/commits/master
