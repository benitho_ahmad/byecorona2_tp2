from django.urls import path
from .views import logIn, logOut

app_name = 'login'

urlpatterns = [
    path('', logIn, name='login'),
    path('logout/', logOut, name='logout')
]