from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm


class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Username", widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'id' : 'signIn-username',
        'placeholder' : 'Username',
        'type' : 'text',
        'required' : True
    }))

    password = forms.CharField(label="Password", widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'id' : 'signIn-password',
        'placeholder' : 'Password',
        'type' : 'password',
        'required' : True
    }))

class Meta:
    model = User
    fields = ['username', 'password']
