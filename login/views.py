from django.shortcuts import render, redirect
from .forms import LoginForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm

# Create your views here.
def logIn(request):
    if request.user.is_authenticated:
        return redirect('/')
    
    else:
        if request.method == "POST":
            form = LoginForm(data = request.POST)

            if form.is_valid():
                usernameInput = request.POST["username"]
                passwordInput = request.POST["password"]
                print(usernameInput, passwordInput)
                user = authenticate(request, username = usernameInput, password = passwordInput)

                if user is not None:
                    login(request, user)
                    return redirect('/')

        else:
            form = LoginForm()
        context = {"form" : form,}
        return render(request, "login.html", context)

def logOut(request):
    logout(request)
    return redirect("/")


