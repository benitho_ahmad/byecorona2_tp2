from django.test import TestCase, Client, tag, LiveServerTestCase
from django.urls import resolve, reverse
from .views import logIn
from django.contrib.auth.models import User

# Create your tests here.

class SignupTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user('uname', password='pass')
        self.login_response_success = Client().post('/login/', {
			'username':'uname', 'password':'pass'
			})
        self.login_response_fail = Client().post('/login/', {
			'username':'uname', 'password':'passSalah'
			})

    def test_login(self):
        self.assertEqual(self.login_response_success.status_code, 302)
        self.assertEqual(self.login_response_fail.status_code, 200)
