from django.test import TestCase, Client, tag, LiveServerTestCase
from django.urls import resolve, reverse
from .views import signup
from django.contrib.auth.models import User
# Create your tests here.

class UrlTest(TestCase):
    def test_url_signup(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/signup.html')

class SignupTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user('uname', password='pass')
        self.signup_response_success = Client().post('/signup/', {
            'username':'chefrakha', 'email':'chefrakha@gmail.com',
            'password1':'alhamdullilah', 'password2':'alhamdullilah'
        })
        self.signup_response_fail = Client().post('/signup/',{
			'username':'chefrakha', 'email':'chefrakha@gmail.com',
			'password1' : 'alhamdullilah', 'password2': 'alhamdullilahhh'
			})


    def testSignup(self):
        self.assertEqual(self.signup_response_success.status_code, 302)
        self.assertEqual(self.signup_response_fail.status_code, 200)
