from django.test import TestCase, Client
from django.urls import resolve, reverse
from http import HTTPStatus
from .models import Message
from .forms import MessageForm
from . import views
import json
from django.utils.encoding import force_text
from django.contrib.auth.models import User


# Create your tests here.
class TestPreventions(TestCase):
#     def test_model_used_in_preventions_html(self):
#         Message.objects.create(message="THIS IS MESSAGE")
#         count_the_message = Message.objects.all().count()
#         self.assertEquals(count_the_message, 1)

    def test_str_equal_to_name(self):
        Message.objects.create(message="THIS IS MESSAGE")
        obj = Message.objects.get(pk=1)
        self.assertEqual(str(obj), obj.name)

    def test_user(self):
        user = User.objects.create(username="dummy", email="dummy@example.com")
        user.set_password("dummy12345")
        user.save()
        self.client.login(
                username="dummy",
                password="dummy12345"
        )

    def test_try_add_message_before_login(self):
        response = self.client.get('/preventions/')
        self.assertContains(response, "Login first")
        self.assertContains(response, "before you share tips!")

    def test_can_save_a_POST_request(self):
        self.test_user()
        response = self.client.get('/preventions/')
        self.assertContains(response, "Message")

#     def test_function_views_preventions_used(self):
#         function = resolve('/preventions/')
#         self.assertEqual(function.func, views.preventions)
        
#     def test_post_success(self):
#         response = self.client.post('/preventions/', data={'name': 'THIS IS NAME', 'message': 'THIS IS MESSAGE'})
#         self.assertEqual(response.status_code, HTTPStatus.FOUND)
#         self.assertEqual(Message.objects.count(), 1)
    
#     def test_pemanggilan_sesuai_keyword(self):
#         response = Client().get('/data/')
#         self.assertEqual(response.status_code, 302)


class TestMessage(TestCase):
    def setUp(self):
        data = Message(
            name="THIS IS NAME",
            message="THIS IS MESSAGE",
        )
        data.save()

    def test_model_preventions(self):
        count = Message.objects.all().count()
        self.assertEquals(count, 1)
    
    def test_get_JSON(self):
        response = self.client.get("/preventions/data/")
        message = Message.objects.all()[::-1][:]
        self.assertEqual(len(json.loads(response.content)['data']), len(message))