from django.db import models

# Create your models here.
class Message(models.Model):
    name = models.CharField(max_length=15)
    message = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    def get_dict(self):
        return {
            'name' : self.name,
            'message' : self.message,
        }