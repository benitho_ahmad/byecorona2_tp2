from django.shortcuts import render, redirect
from .models import Message
from .forms import MessageForm
from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
import requests
import json

def preventions(request):
    responses = {}
    if request.method == 'POST':
        if request.is_ajax():
            if request.user.is_authenticated:
                form = MessageForm(request.POST)
                if form.is_valid():
                    name = request.user.username
                    message = form.cleaned_data['message']
                    Message(name=name, message = message).save()
                    return JsonResponse({'success': True})
        return JsonResponse({'success': False})

    else:
        form = MessageForm()
        responses['form'] = form
        return render(request, 'preventions/preventions.html', responses)

def data(request):
    data = {
        'data' : [i.get_dict() for i in Message.objects.all()[::-1][:]],
    }
    return JsonResponse(data=data)