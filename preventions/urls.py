from django.urls import path

from . import views

app_name = 'preventions'

urlpatterns = [
    path('', views.preventions, name='preventions'),
    path('data/', views.data, name='data'),
]