from django.forms import ModelForm
from django import forms
from .models import Message

class MessageForm(forms.ModelForm):
	class Meta:
		model = Message
		fields = (
		    'message',
		)

		widgets = {
			'message': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Enter Your Message'}),
		}