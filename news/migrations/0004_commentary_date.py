# Generated by Django 3.1.3 on 2020-11-14 09:37

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0003_auto_20201113_2302'),
    ]

    operations = [
        migrations.AddField(
            model_name='commentary',
            name='date',
            field=models.DateTimeField(blank=True, default=datetime.datetime.now),
        ),
    ]
