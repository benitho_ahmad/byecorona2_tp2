from django.shortcuts import render, redirect
from .forms import NewsCommentary
from .models import Commentary

# Create your views here.
def newsPage(request):
    commentaries = Commentary.objects.all()

    if request.method == 'POST' :
        comment = Commentary(name = request.POST['name'], comment = request.POST['comment'])
        comment.save()
        return redirect('news:newsPage')
    
    context = {'commentaries' : commentaries}
    
    return render(request, 'news/news.html', context)
