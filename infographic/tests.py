from django.test import LiveServerTestCase, TestCase, Client, tag
from django.urls import resolve, reverse
from .models import status
from . import views

# Create your tests here.

class TestInfographic(TestCase):
    def test_url_infografis_main(self):
        response = Client().get('/infografis/')
        self.assertEquals(response.status_code, 200)

    def test_cek_hasil_template_infografis(self):
        response = Client().get('/infografis/')
        self.assertTemplateUsed(response, 'main/infografis.html')

    def test_function_views_used(self):
        function = resolve('/infografis/')
        self.assertEqual(function.func, views.infografis)

    def test_model_status(self):
        status.objects.create(infected='999', recovery='999', death='999')
        check_masuk = status.objects.count()
        self.assertEquals(check_masuk, 1)