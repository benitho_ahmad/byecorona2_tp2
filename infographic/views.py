from django.shortcuts import render
from django.http import HttpResponse
from .models import status
from django.http import JsonResponse
import requests
import json

# Create your views here.
def infografis(request):
    statusharian = status.objects.all()
    re = {'status':statusharian}
    return render(request, 'main/infografis.html', re)

def fungsi_data(request):
    url = "https://covid-19-data.p.rapidapi.com/country/code"
    querystring = {"code":"idn"}
    headers = {
        'x-rapidapi-key': "15e4c3e120msh0411571466a1b09p1e4905jsn060841a14cdf",
        'x-rapidapi-host': "covid-19-data.p.rapidapi.com"
        }
    response = requests.request("GET", url, headers=headers, params=querystring)
    print(response.text)
    data = json.loads(response.text)
    return JsonResponse(data, safe=False)



