from django.urls import path

from . import views

app_name = 'infographic'

urlpatterns = [
    path('infografis/', views.infografis, name='infografis'),
    path('ambilData/', views.fungsi_data, name='fungsi_data')
]
