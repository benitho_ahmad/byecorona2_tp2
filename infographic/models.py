from django.db import models

# Create your models here.
class status(models.Model):
    infected = models.CharField(max_length=50)
    recovery = models.CharField(max_length=50)
    death = models.CharField(max_length=50)
